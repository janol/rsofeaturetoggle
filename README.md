# RSOFeatureToggle

`RSOFeatureToggle` is a small library to provide runtime switches during development. This was developed to support trunk based development.

### How do I get set up?

Run `pods update` to be able to execute tests on this project.

Including this lib into your project contains a single instance of `RSOFeatureToggleStore`. Look at the tests, to see examples on how to use it.

## Licenses
`RSOFeatureToggle` is available under the MIT license. See the LICENSE file for more info.

Thanks to:

* [Kiwi](https://github.com/kiwi-bdd/Kiwi), [License](https://github.com/kiwi-bdd/Kiwi/blob/master/License.txt)