//
//  RSOFeatureToggleStore.h
//  RSOFeatureToggle
//
//  Created by Jan Olbrich on 20.03.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSOFeature.h"


@interface RSOFeatureToggleStore : NSObject

- (void)setFeatureEnvironment:(RSOFeatureEnvironment)environment;
- (BOOL)isFeatureEnabled:(NSString *)featureName;
- (void)addFeature:(NSString *)featureName toEnvironment:(RSOFeatureEnvironment)environment;
- (void)setEnabled:(BOOL)enabled forFeature:(NSString *)featureName;
- (void)resetFeature:(NSString *)featureName;
- (NSArray *)features;

@end
