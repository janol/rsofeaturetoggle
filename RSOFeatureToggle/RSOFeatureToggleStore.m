//
//  RSOFeatureToggleStore.m
//  RSOFeatureToggle
//
//  Created by Jan Olbrich on 20.03.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import "RSOFeatureToggleStore.h"

@interface RSOFeatureToggleStore ()

@property (nonatomic) RSOFeatureEnvironment environment;
@property (nonatomic, strong) NSMutableDictionary *featureStore;
@property (nonatomic, strong) NSMutableSet *forcedFeatures;

@end

@implementation RSOFeatureToggleStore

- (NSMutableDictionary *)featureStore {
    if (!_featureStore) {
        _featureStore = [NSMutableDictionary new];
    }
    return _featureStore;
}

- (NSMutableSet *)forcedFeatures {
    if (!_forcedFeatures) {
        _forcedFeatures = [NSMutableSet new];
    }
    return _forcedFeatures;
}

- (void)setFeatureEnvironment:(RSOFeatureEnvironment)environment {
    self.environment = environment;
}

- (BOOL)isFeatureEnabled:(NSString *)featureName {
    RSOFeature *feature = [self.featureStore valueForKey:featureName];
    if ([self.forcedFeatures member:feature]) {
        return feature.enabled;
    }

    return [feature isInEnvironment:self.environment];
}

- (void)addFeature:(RSOFeature *)feature {
    feature.enabled = [feature isInEnvironment:self.environment];
    [self.featureStore setObject:feature forKey:feature.featureName];
}

- (void)addFeature:(NSString *)featureName toEnvironment:(RSOFeatureEnvironment)environment {
    [self addFeature:[[RSOFeature alloc] initWithName:featureName andEnvironment:environment]];
}

- (void)setForcedFeature:(RSOFeature *)feature withValue:(BOOL)enabled {
    feature.enabled = enabled;
    [self.forcedFeatures addObject:feature];
}

- (void)setEnabled:(BOOL)enabled forFeature:(NSString *)featureName {
    RSOFeature *forcedFeature = [self.featureStore objectForKey:featureName];
    [self setForcedFeature:forcedFeature withValue:enabled];
}

- (void)resetFeature:(NSString *)featureName {
    RSOFeature *feature = [self.featureStore objectForKey:featureName];
    [self.forcedFeatures removeObject:feature];
}

- (NSArray *)features {
    return [self.featureStore allValues];
}

@end
