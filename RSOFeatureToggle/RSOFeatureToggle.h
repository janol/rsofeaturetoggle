//
//  RSOFeatureToggle.h
//  RSOFeatureToggle
//
//  Created by Jan Olbrich on 20.03.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RSOFeatureToggle.
FOUNDATION_EXPORT double RSOFeatureToggleVersionNumber;

//! Project version string for RSOFeatureToggle.
FOUNDATION_EXPORT const unsigned char RSOFeatureToggleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RSOFeatureToggle/PublicHeader.h>

#import "RSOFeatureToggle/RSOFeature.h"
#import "RSOFeatureToggle/RSOFeatureToggleStore.h"


