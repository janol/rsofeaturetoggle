//
//  RSOFeature.m
//  RSOFeatureToggle
//
//  Created by Jan Olbrich on 21.03.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import "RSOFeature.h"

@implementation RSOFeature

-(instancetype)initWithName:(NSString *)name andEnvironment:(RSOFeatureEnvironment)environment {
    self = [super init];
    if (self) {
        self.featureName = name;
        self.featureEnvironment = environment;
        self.enabled = YES;
    }
    
    return self;
}

- (BOOL)isInEnvironment:(RSOFeatureEnvironment)environment {
    return self.featureEnvironment <= environment;
}

@end
