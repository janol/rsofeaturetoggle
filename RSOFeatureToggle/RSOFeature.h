//
//  RSOFeature.h
//  RSOFeatureToggle
//
//  Created by Jan Olbrich on 21.03.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RSOFeatureEnvironment) {
    RSO_ENVIRONMENT_RELEASE = 0,
    RSO_ENVIRONMENT_BETA,
    RSO_ENVIRONMENT_DEVELOPMENT
};

@interface RSOFeature : NSObject

- (instancetype)initWithName:(NSString *)name andEnvironment:(RSOFeatureEnvironment)environment;
- (BOOL)isInEnvironment:(RSOFeatureEnvironment)environment;

@property (nonatomic) RSOFeatureEnvironment featureEnvironment;
@property (nonatomic, strong) NSString *featureName;
@property (nonatomic) BOOL enabled;

@end
