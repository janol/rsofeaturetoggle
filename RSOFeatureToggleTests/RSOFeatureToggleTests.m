//
//  RSOFeatureToggleTests.m
//  RSOFeatureToggleTests
//
//  Created by Jan Olbrich on 20.03.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "RSOFeatureToggleStore.h"

SPEC_BEGIN(FeatureToggle)

describe(@"The FeatureToggleStore", ^{
    __block RSOFeatureToggleStore *sut;
    
    beforeEach(^{
        sut = [[RSOFeatureToggleStore alloc] init];
        [sut addFeature:@"awesomeFeature1" toEnvironment:RSO_ENVIRONMENT_RELEASE];
        [sut addFeature:@"awesomeFeature2" toEnvironment:RSO_ENVIRONMENT_BETA];
        [sut addFeature:@"awesomeFeature3" toEnvironment:RSO_ENVIRONMENT_DEVELOPMENT];
    });
    
    context(@"after initial setup", ^{
        
        afterEach(^{
            sut = nil;
        });
        
        it(@"should not be nil", ^{
            [[sut shouldNot] beNil];
        });
    });
    
    context(@"having one feature under release", ^{
        beforeEach(^{
            [sut setFeatureEnvironment:RSO_ENVIRONMENT_RELEASE];
        });
        
        afterEach(^{
            sut = nil;
        });
        
        it(@"should return yes for awesomeFeature1 under release", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature1"]) should] beYes];
        });
        
        it(@"should return no for awesomeFeature2 under release", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature2"]) should] beNo];
        });
        
        it(@"should return no for awesomeFeature3 under release", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature3"]) should] beNo];
        });
    });
    
    context(@"having one feature in beta", ^{
        beforeEach(^{
            [sut setFeatureEnvironment:RSO_ENVIRONMENT_BETA];
        });
        
        afterEach(^{
            sut = nil;
        });
        
        it(@"should return yes for awesomeFeature2 in beta", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature2"]) should] beYes];
        });
        
        it(@"should return yes for awesomeFeature1 in beta", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature1"]) should] beYes];
        });
        
        it(@"should return no for awesomeFeature3 in beta", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature3"]) should] beNo];
        });
    });
    
    context(@"having one feature under development", ^{
        beforeEach(^{
            [sut setFeatureEnvironment:RSO_ENVIRONMENT_DEVELOPMENT];
        });
        
        afterEach(^{
            sut = nil;
        });
        
        it(@"should return yes for awesomeFeature2 in beta", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature2"]) should] beYes];
        });
        
        it(@"should return yes for awesomeFeature1 in beta", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature1"]) should] beYes];
        });
        
        it(@"should return no for awesomeFeature3 in beta", ^{
            [[theValue([sut isFeatureEnabled:@"awesomeFeature3"]) should] beYes];
        });
    });
    
    context(@"controling single features", ^{
        beforeEach(^{
            [sut setFeatureEnvironment:RSO_ENVIRONMENT_RELEASE];
        });
        
        it(@"should activate awesomeFeature2 under release", ^{
            [sut setEnabled:YES forFeature:@"awesomeFeature2"];
            
            [[theValue([sut isFeatureEnabled:@"awesomeFeature2"]) should] beYes];
        });
        
        it(@"should deactivate awesomeFeature1 under release", ^{
            [sut setEnabled:NO forFeature:@"awesomeFeature1"];
            
            [[theValue([sut isFeatureEnabled:@"awesomeFeature1"]) should] beNo];
        });
        
        it(@"should be enable awesomeFeature1 under release, even though it was disabled", ^{
            [sut setEnabled:NO forFeature:@"awesomeFeature1"];
            [sut setEnabled:YES forFeature:@"awesomeFeature1"];
            
            [[theValue([sut isFeatureEnabled:@"awesomeFeature1"]) should] beYes];
        });
        
        it(@"should be disable awesomeFeature2 under release, even though it was first enabled", ^{
            [sut setEnabled:YES forFeature:@"awesomeFeature2"];
            [sut setEnabled:NO forFeature:@"awesomeFeature2"];
            
            [[theValue([sut isFeatureEnabled:@"awesomeFeature2"]) should] beNo];
        });
        
        it(@"should reset awesomFeature2 under release", ^{
            [sut setEnabled:YES forFeature:@"awesomeFeature2"];
            [sut resetFeature:@"awesomeFeature2"];
            
            [[theValue([sut isFeatureEnabled:@"awesomeFeature2"]) should] beNo];
        });
    });
    
    context(@"displaying all features", ^{
        
        beforeEach(^{
            sut = [[RSOFeatureToggleStore alloc] init];
            [sut setFeatureEnvironment:RSO_ENVIRONMENT_RELEASE];
            [sut addFeature:@"awesomeFeature1" toEnvironment:RSO_ENVIRONMENT_RELEASE];
        });
        
        it(@"should return a list of allFeatures", ^{
            NSArray *features = [sut features];
            
            [[theValue([features count]) should] equal:theValue(1)];
        });
        
        it(@"should return enabled as yes when not forced", ^{
            NSArray *features = [sut features];
            RSOFeature *feature = features[0];
            [[theValue(feature.enabled) should] beYes];
        });
        
        it(@"should return enabled as yes when forced", ^{
            [sut setEnabled:NO forFeature:@"awesomeFeature1"];
            NSArray *features = [sut features];
            RSOFeature *feature = features[0];
            [[theValue(feature.enabled) should] beNo];
        });
        
        it(@"should return enabled as no for awesomeFeature2 in beta not forced", ^{
            [sut addFeature:@"awesomeFeature2" toEnvironment:RSO_ENVIRONMENT_BETA];
            NSArray *features = [sut features];
            RSOFeature *feature = features[1];
            [[theValue(feature.enabled) should] beNo];
        });
        
        it(@"should return enabled as yes for awesomeFeature2 in beta when forced", ^{
            [sut addFeature:@"awesomeFeature2" toEnvironment:RSO_ENVIRONMENT_BETA];
            [sut setEnabled:YES forFeature:@"awesomeFeature2"];
            NSArray *features = [sut features];
            RSOFeature *feature = features[1];
            [[theValue(feature.enabled) should] beYes];
        });
    });
});

SPEC_END