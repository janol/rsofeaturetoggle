//
//  RSOFeatureTest.m
//  RSOFeatureToggle
//
//  Created by Jan Olbrich on 21.03.15.
//  Copyright (c) 2015 Jan Olbrich. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "RSOFeature.h"

SPEC_BEGIN(RSOFeatureTest)

describe(@"The RSOFeature", ^{
    
    __block RSOFeature *sut;
    
    context(@"after initialization", ^{
        
        beforeEach(^{
            sut = [[RSOFeature alloc] initWithName:@"awesomeFeature1" andEnvironment:RSO_ENVIRONMENT_BETA];
        });
        
        afterEach(^{
            sut = nil;
        });
        
        it(@"should not be nil", ^{
            [[sut shouldNot] beNil];
        });
        
        it(@"should return release as environment", ^{
            sut = [[RSOFeature alloc] initWithName:@"awesomeFeature1" andEnvironment:RSO_ENVIRONMENT_RELEASE];
            [[theValue(sut.featureEnvironment) should] equal:theValue(RSO_ENVIRONMENT_RELEASE)];
        });
        
        it(@"should return beta as environment", ^{
            [[theValue(sut.featureEnvironment) should] equal:theValue(RSO_ENVIRONMENT_BETA)];
        });
        
        it(@"should return awesomeFeature1 for name", ^{
            [[sut.featureName should] equal:@"awesomeFeature1"];
        });
    });
    
    context(@"when in beta", ^{
        
        beforeEach(^{
            sut = [[RSOFeature alloc] initWithName:@"awesomeFeature1" andEnvironment:RSO_ENVIRONMENT_BETA];
        });
        
        afterEach(^{
            sut = nil;
        });
        
        it(@"should be part of development", ^{
            [[theValue([sut isInEnvironment:RSO_ENVIRONMENT_DEVELOPMENT]) should] beYes];
        });
        
        it(@"should be part of beta", ^{
            [[theValue([sut isInEnvironment:RSO_ENVIRONMENT_BETA]) should] beYes];
        });
        
        it(@"should not be part of release", ^{
            [[theValue([sut isInEnvironment:RSO_ENVIRONMENT_RELEASE]) should] beNo];
        });
    });
    
    context(@"with enable switch", ^{
        beforeEach(^{
            sut = [[RSOFeature alloc] initWithName:@"awesomeFeature1" andEnvironment:RSO_ENVIRONMENT_BETA];
        });
        
        afterEach(^{
            sut = nil;
        });
        
        it(@"should return YES for enabled", ^{
            [[theValue(sut.enabled) should] beYes];
        });
        
        it(@"should return NO for enabled", ^{
            sut.enabled = NO;
            [[theValue(sut.enabled) should] beNo];
        });
    });
});

SPEC_END